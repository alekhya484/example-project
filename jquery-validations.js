$.validator.setDefaults({
  highlight: function(element) {
    $(element).closest('.form-group').addClass('has-error');
  },
  unhighlight: function(element) {
    $(element).closest('.form-group').removeClass('has-error');
  },
  errorElement: 'small',
  errorClass: 'help-block',
  errorPlacement: function(error, element) {
    if (element.parent('.input-group').length) {
      error.insertAfter(element.parent());
      $(element).parent('.input-group').next('small').next('small').remove();
    } else {
      error.insertAfter(element);
      $(element).next('small').next('small').remove();
    }
    $(loadingButton).button('reset');
  }
});

jQuery.validator.addMethod("youtubevimeo", function(value, element) {
  return this.optional(element) || /(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/i.test(value);
}, "Please enter a valid youtube or vimeo video URL");

jQuery.validator.addMethod("dateIsFuture", function(value, element) {
  var now = moment.utc().startOf('day').toDate();
  var myDate = moment.utc(value).startOf('day').toDate();
  return this.optional(element) || myDate >= now;
});
jQuery.validator.addMethod("emailaddress", function(value, element) {
  return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
}, "Please enter valid email");

jQuery.validator.addMethod("userfullName", function(value, element) {
  var array = value.split(' ');
  return this.optional(element) || array.length > 1;
}, "Please enter yourname");


jQuery.validator.addMethod("allowSpecial", function(value, element) {

  //allowing spaces,singlequotes,underscore and hyphen
  var value = /[^A-Za-z0-9\s\-\'\_]/.test(value);
  return !value;
}, "Title does not allow special characters");

jQuery.validator.addMethod("limit365Days", function(value, element) {
  var inOneYear = moment().add(365, 'days');
  var myDate = new Date(value);
  return this.optional(element) || myDate < inOneYear;
});

jQuery.validator.addMethod('dateAfter', function(value, element, params) {
  // if start date is valid, validate it as well
  var start = $(params);
  if (!start.data('validation.running')) {
    $(element).data('validation.running', true);
    setTimeout($.proxy(

      function() {
        this.element(start);
      }, this), 0);
    setTimeout(function() {
      $(element).data('validation.running', false);
    }, 0);
  }
  return this.optional(element) || this.optional(start[0]) || moment.utc(value).startOf('day').toDate() >= moment.utc($(params).val()).startOf('day').toDate();
}, 'Must be after corresponding start date');
jQuery.validator.addMethod('dateBefore', function(value, element, params) {
  var start = $(params);
  if (!start.data('validation.running')) {
    $(element).data('validation.running', true);
    setTimeout($.proxy(

      function() {
        this.element(start);
      }, this), 0);
    setTimeout(function() {
      $(element).data('validation.running', false);
    }, 0);
  }
  return this.optional(element) || this.optional(start[0]) || moment.utc(value).startOf('day').toDate() <= moment.utc($(params).val()).startOf('day').toDate();
}, 'Must be before corresponding end date');

jQuery.validator.addMethod("valueNotEquals", function(value, element, arg) {
  if ($(element).find('option').length > 1) {
    return arg != value;
  } else {
    return true;
  }
}, "Please Select a Value");

jQuery.validator.addMethod("money", function(value, element) {
  return this.optional(element) || /^\$?[0-9][0-9\,]*(\.\d{1,2})?$|^\$?[\.]([\d][\d]?)$/.test(value);
}, "Please provide a valid dollar amount (up to 2 decimal places)");
jQuery.validator.addMethod("phoneAll", function(phone_number, element) {
  phone_number = phone_number.replace(/\s+/g, "");
  return this.optional(element) || phone_number.length > 6
}, "Please specify a valid phone number");
jQuery.validator.addMethod("zipcodeAll", function(country, value, element) {
  var e = country;
  var g = $($(element + " option[value=" + $(element).val() + "]")[0]).text();
  switch (g) {
    case "Austria":
      h = /^([1-9]{1})(\d{3})$/.test(e);
      break;
    case "Bulgaria":
      h = /^([1-9]{1}[0-9]{3})$/.test(a.trim(e));
      break;
    case "Brazil":
      h = /^(\d{2})([\.]?)(\d{3})([\-]?)(\d{3})$/.test(e);
      break;
    case "Canada":
      h = /^(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|X|Y){1}[0-9]{1}(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|W|X|Y|Z){1}\s?[0-9]{1}(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|W|X|Y|Z){1}[0-9]{1}$/i.test(e);
      break;
    case "Switzerland":
      h = /^([1-9]{1})(\d{3})$/.test(e);
      break;
    case "Czech Republic":
      h = /^(\d{3})([ ]?)(\d{2})$/.test(e);
      break;
    case "Germany":
      h = /^(?!01000|99999)(0[1-9]\d{3}|[1-9]\d{4})$/.test(e);
      break;
    case "Denmark":
      h = /^(DK(-|\s)?)?\d{4}$/i.test(e);
      break;
    case "Spain":
      h = /^(?:0[1-9]|[1-4][0-9]|5[0-2])\d{3}$/.test(e);
      break;
    case "France":
      h = /^[0-9]{5}$/i.test(e);
      break;
    case "United Kingdom":
      h = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/.test(e);
      break;
    case "India":
      h = /^\d{3}\s?\d{3}$/.test(e);
      break;
    case "Ireland":
      h = /^(D6W|[ACDEFHKNPRTVWXY]\d{2})\s[0-9ACDEFHKNPRTVWXY]{4}$/.test(e);
      break;
    case "Italy":
      h = /^(I-|IT-)?\d{5}$/i.test(e);
      break;
    case "Morocco":
      h = /^[1-9][0-9]{4}$/i.test(e);
      break;
    case "Netherlands":
      h = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i.test(e);
      break;
    case "Poland":
      h = /^[0-9]{2}\-[0-9]{3}$/.test(e);
      break;
    case "Portugal":
      h = /^[1-9]\d{3}-\d{3}$/.test(e);
      break;
    case "Romania":
      h = /^(0[1-8]{1}|[1-9]{1}[0-5]{1})?[0-9]{4}$/i.test(e);
      break;
    case "Russia":
      h = /^[0-9]{6}$/i.test(e);
      break;
    case "Sweden":
      h = /^(S-)?\d{3}\s?\d{2}$/i.test(e);
      break;
    case "Singapore":
      h = /^([0][1-9]|[1-6][0-9]|[7]([0-3]|[5-9])|[8][0-2])(\d{4})$/i.test(e);
      break;
    case "Slovakia":
      h = /^(\d{3})([ ]?)(\d{2})$/.test(e);
      break;
    case "United States":
      h = /^\d{5}(-\d{4})?$/.test(e);
      break;
    default:
      h = /^\d{4,5}([\-]?\d{4})?$/.test(e)
  }
  return h;
}, "Please enter a valid postal code");

/*jQuery.validator.addMethod("ein", function(value, element) {
  return this.optional(element) || /^(\d{3})-?\d{2}-?\d{4}$/i.test(value) || /^(\d{2})-?\d{7}$/i.test(value)

}, "Invalid Tax ID.  Your EIN must be exactly 9 numbers.");*/
$.validator.addMethod("cus_url", function(value, element) {
  if (value.substr(0, 7) != 'http://' && value.substr(0, 8) != 'https://') {
    value = 'http://' + value;
  }
  if (value.substr(value.length - 1, 1) != '/') {
    value = value + '/';
  }
  return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
}, "Not valid url.");
$.validator.addMethod("validAmount", function(value, element) {
  var amount = false;
  if (Number(value) > 0) {
    amount = true;
  }
  return amount;
}, "enter valid amount");
$.validator.addMethod("validQuantity", function(value, element) {
  var amount = true;
  if (Number(value) < 0) {
    amount = false;
  }
  return amount;
}, "Quantity never be negative");
$.validator.addMethod("phoneValid", function(value, element) {
  return this.optional(element) || /^[1-9]\d*(\.\d+)?$/.test(value)

}, "enter valid Number");

jQuery.validator.addMethod("multiemails", function(value, element) {
  if (this.optional(element)) // return true on optional element
    return true;
  var emails = value.split(/[;,]+/); // split element by , and ;
  valid = true;
  for (var i in emails) {
    value = emails[i];
    valid = valid &&
      jQuery.validator.methods.email.call(this, $.trim(value), element);
  }
  return valid;
}, "Email addresses entered are not valid");

function _gb(a) {
  for (var b = "[ABCDEFGHIJKLMNOPRSTUWYZ]", c = "[ABCDEFGHKLMNOPQRSTUVWXY]", d = "[ABCDEFGHJKPMNRSTUVWXY]", e = "[ABEHMNPRVWXY]", f = "[ABDEFGHJLNPQRSTUWXYZ]", g = [new RegExp("^(" + b + "{1}" + c + "?[0-9]{1,2})(\\s*)([0-9]{1}" + f + "{2})$", "i"), new RegExp("^(" + b + "{1}[0-9]{1}" + d + "{1})(\\s*)([0-9]{1}" + f + "{2})$", "i"), new RegExp("^(" + b + "{1}" + c + "{1}?[0-9]{1}" + e + "{1})(\\s*)([0-9]{1}" + f + "{2})$", "i"), new RegExp("^(BF1)(\\s*)([0-6]{1}[ABDEFGHJLNPQRST]{1}[ABDEFGHJLNPQRSTUWZYZ]{1})$", "i"), /^(GIR)(\s*)(0AA)$/i, /^(BFPO)(\s*)([0-9]{1,4})$/i, /^(BFPO)(\s*)(c\/o\s*[0-9]{1,3})$/i, /^([A-Z]{4})(\s*)(1ZZ)$/i, /^(AI-2640)$/i], h = 0; h < g.length; h++)
    if (g[h].test(a)) return !0;
  return !1
}
var validatorRules = {};
validatorRules.getLoginRules = function() {
  return {
    rules: {
      email: {
        required: true,
        email: true
      },
      password: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      },
      password: {
        required: "Password is required"
      }
    }
  };
};
validatorRules.getSignupRules = function() {
  return {
    rules: {
      firstname: {
        required: true
      },
      lastname: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true
      },
      confirmpassword: {
        required: true,
        equalTo: "#password"
      },
      acceptTerms: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      acceptTerms: {
        required: "You must check that you agree to our terms and privacy policy."
      }
    },
    errorPlacement: function(error, element) {
      if (element.is("#acceptTerms")) {
        error.appendTo('#acceptTermsError');
      }
    }
  };
};

validatorRules.getDonorProfileUpdateRules = function() {
  return {
    rules: {
      name: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      profile_pic_url: {
        required: false,
        accept: "image/*"
      },
      about_me: {
        required: true
      },
      postal_code: {
        required: false,
        //country:'#country',
        zipcodeAll: '#country'
      },
      phone: {
        required: false,
        phoneAll: true
      },
      city: {
        required: false,
      },
      state: {
        // required: true,
        // valueNotEquals: "select"
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      address_1: {
        required: true
      },
      relationship: {
        required: false,
        //valueNotEquals: "select"
      },
      religious_affiliation: {
        required: false,
        //valueNotEquals: "select"
      },
      slug: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      name: {
        required: "Please enter your first and last name"
      },
      email: {
        required: "Your email address is required"
      },
      about_me: {
        maxlength: "Profile description cannot exceed 333 characters",
        required: "Profile description is required"
      },
      phone: {
        required: "phone number is required"
      },
      postal_code: {
        required: "Please enter a valid postal code"
      },
      address_1: {
        required: "Address1 is required"
      }
    }
  };
};

validatorRules.getResetPasswordRules = function() {
  return {
    rules: {
      password: {
        required: true,
        minlength: 6
      },
      confirmPassword: {
        required: true,
        equalTo: "#js-password"
      },
      onkeyup: false
    },
    messages: {
      password: {
        minlength: "Password is too short (minimum is 6 characters)"
      }
    }
  };
};

validatorRules.getCreateNewCampaignRules = function() {

  return {
    rules: {
      title: {
        required: true
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      code_picture_url: {
        required: true,
        accept: "image/*"
      },
      code_video_url: {
        youtubevimeo: true
      },
      description: {
        required: true
      },
      suggested_donation: {
        required: true,
        money: true
      },
      goal: {
        required: true,
        money: true
      },
      // type: {
      //   required: true
      // },
      start_date: {
        required: true,
        dateIsFuture: true
          //date: true
      },
      end_date: {
        required: true,
        dateAfter: '#start_date',
        limit365Days: true
          //    date: true
      },
      address_1: {
        required: false
      },
      address_2: {
        required: false
      },
      state: {
        required: false,
        valueNotEquals: "select"
      },
      country: {
        required: false,
        valueNotEquals: "select"
      },
      campaign_zip: {
        required: true,
        //zipcodeUS: true,
        valueNotEquals: "",
        zipcodeAll: '#country'
      },
      onkeyup: false
    },
    messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Campaign title is required"
      },
      start_date: {
        dateIsFuture: "Start date cannot be in the past",
        required: "Start date is required for event campaigns"
      },
      end_date: {
        dateAfter: "End date must be after the start date",
        required: "End date is required for event campaigns",
        limit365Days: "End date cannot be more than one year from today"
      },
      goal: {
        required: "Campaign goal amount is required",
      },
      code_video_url: {
        youtubevimeo: "This is not a valid video URL"
      },
      description: {
        required: "Campaign description is required"
      },
      campaign_zip: {
        zipcodeUS: "Zip code is not valid"
      }
    }
  };
};

validatorRules.getPostRules = function() {
  return {
    rules: {
      postmessage: {
        required: true
      },
      onkeyup: false
    }
  };
};

validatorRules.validateAddOrganizationForm = function() {
  return {
    rules: {
      charity_name: {
        required: true
      },
      slug: {
        required: true
      },
      web_url: {
        required: true,
        cus_url: true
      },
      city: {
        required: true
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      state: {
        required: true
      },
      postal_code: {
        required: true,
        zipcodeAll: '#country'
      },
      email: {
        required: true,
        email: true
      },
      onkeyup: false
    },
    messages: {
      web_url: {
        required: "Please enter your Organization URL",
        cus_url: "Please enter your Organization URL"
      },
      charity_name: {
        required: "Please enter your Organization Name"
      },
      city: {
        required: "Please enter your City Name"
      },
    }
  };
};

validatorRules.validateEventCreationForm1 = function() {
  return {
    rules: {
      event_name: {
        required: true
      },
      organizer: {
        required: true
      },
      start_date: {
        required: true,
        dateIsFuture: true
          //date: true
      },
      end_date: {
        required: true,
        dateAfter: '#start_date',
        dateIsFuture: true,
        limit365Days: true
          //date: true
      },
      event_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      onkeyup: false
    },
    messages: {
      event_name: {
        required: "Event title is required"
      },
      organizer: {
        required: "Event organiser name is required"
      },
      start_date: {
        dateIsFuture: "Start date cannot be in the past",
        required: "Start date is required for event"
      },
      end_date: {
        dateIsFuture: "End date cannot be in the past",
        dateAfter: "End date must be after the start date",
        required: "End date is required for event",
        limit365Days: "End date cannot be more than one year from today"
      },
      event_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      }
    }
  };
}
validatorRules.updateEventCreationForm1 = function() {
  return {
    rules: {
      event_name: {
        required: true
      },
      organizer: {
        required: true
      },
      start_date: {
        required: true
          // dateIsFuture: true
          //date: true
      },
      end_date: {
        required: true,
        dateAfter: '#start_date',
        dateIsFuture: true,
        limit365Days: true
          //date: true
      },
      event_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      onkeyup: false
    },
    messages: {
      event_name: {
        required: "Event title is required"
      },
      organizer: {
        required: "Event organiser name is required"
      },
      start_date: {
        // dateIsFuture: "Start date cannot be in the past",
        required: "Start date is required for event"
      },
      end_date: {
        dateIsFuture: "End date cannot be in the past",
        dateAfter: "End date must be after the start date",
        required: "End date is required for event",
        limit365Days: "End date cannot be more than one year from today"
      },
      event_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      }
    }
  };
}
validatorRules.validateEventCreationForm2 = function() {
  return {
    rules: {
      event_picture_url: {
        required: true
      },
      event_video_url: {
        youtubevimeo: true
      }
    },
    messages: {
      event_picture_url: {
        required: "Event image is required"
      },
      event_video_url: {
        youtubevimeo: "This is not a valid video URL"
      }
    }
  };
}

validatorRules.validateAddOrganizationNewUserForm = function() {
  return {
    rules: {
      charity_name: {
        required: true
      },
      ein: {
        required: true,
        number: true
      },
      web_url: {
        required: true,
        cus_url: true
      },
      city: {
        required: true
      },
      state: {
        required: true,
        valueNotEquals: "select"
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      postal_code: {
        required: true,
        zipcodeAll: '#country'
      },
      email: {
        required: true,
        email: true
      },
      yourname: {
        required: true,
        userfullName: true
      },
      phone_number: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      web_url: {
        required: "Please enter your Organization URL",
        cus_url: "Please enter your Organization URL"
      },
      charity_name: {
        required: "Please enter your Organization Name"
      },
      city: {
        required: "Please enter your City Name"
      },
      yourname: {
        required: "Please enter yourname"
      }
    }
  };
}
validatorRules.getEditCharityRules = function() {
  return {
    rules: {
      full_description: {
        required: true,
        maxlength: 500
      },
      title: {
        required: true,
      },
      slug: {
        required: true,
      },
      web_url: {
        required: true,
        cus_url: true
      },
      category: {
        required: true,
        valueNotEquals: ""
      },
      onkeyup: false
    },
    messages: {
      full_description: {
        required: "Please enter a description.",
        maxlength: "Description cannot exceed 500 characters."
      },
      slug: {
        required: "Please enter a username."
      },
      web_url: {
        required: "Please enter your organization's URL.",
        cus_url: "This is not a valid a URL.  It should start with http://"
      },
      category: {
        valueNotEquals: "Please select at least one category."
      }
    }
  };
};

validatorRules.getUpdateCampaignRules = function() {
  return {
    rules: {
      title: {
        required: true
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      suggested_donation: {
        required: true,
        money: true
      },
      goal: {
        required: true,
        money: true
      },
      start_date: {
        //required: true,
        dateIsFuture: true
          //date: true
      },
      end_date: {
        required: true,
        //dateAfter: '#start_date',
        dateIsFuture: true,
        limit365Days: true
          //date: true
      },
      address_1: {
        required: false
      },
      code_video_url: {
        youtubevimeo: true
      },
      campaign_zip: {
        required: false,
        //zipcodeUS: true
        zipcodeAll: '#country'
      },
      state: {
        required: true,
        valueNotEquals: "select"
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      onkeyup: false
    },
    messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Campaign title is required"
      },
      start_date: {
        dateIsFuture: "Start date cannot be in the past",
        required: "Start date is required for event campaigns"
      },
      end_date: {
        dateIsFuture: "End date cannot be in the past",
        dateAfter: "End date must be after the start date",
        required: "End date is required for event campaigns",
        limit365Days: "End date cannot be more than one year from today"
      },
      goal: {
        required: "Campaign goal amount is required"
      },
      description: {
        required: "Campaign description is required"
      },
      campaign_zip: {
        zipcodeUS: "Zip code is not valid"
      },
      code_video_url: {
        youtubevimeo: "This is not a valid video URL"
      }
    }
  };
};

validatorRules.getPasswordRules = function() {
  return {
    rules: {
      currentPassword: {
        required: true
      },
      newPassword: {
        required: true,
        minlength: 6
      },
      verifyPassword: {
        required: true,
        equalTo: "#newPassword"
      },
      onkeyup: false,
    },
    messages: {
      newPassword: {
        minlength: "Password is too short (minimum is 6 characters)"
      }
    }
  };
};

validatorRules.getforgetPasswordRules = function() {
  return {
    rules: {
      userEmail: {
        required: true,
        email: true
      },
      onkeyup: false
    }
  };
};
validatorRules.addGivingLevelsRules = function() {
  return {
    rules: {
      title: {
        required: true,
        maxlength: 15,
        allowSpecial:true
      },
      amount: {
        required: true,
        validAmount: true
      },
      quantity: {
        validQuantity: true

      },

      description: {
        required: true,
        maxlength: 50
      },
      onkeyup: false
    },
    messages: {
      title: {
        required: "Title is required",
        maxlength: "Title can't be exceed 15",
        allowSpecial: "Title does not allows special characters"
      },
      amount: {
        required: "Amount is required",
        validAmount: "the checkout amount can't be less than $0.99"
      },
      description: {
        required: "Description is required",
        maxlength: "Text can't be exceed 50"
      },
      qunatity: {
        validQuantity: "Quantity never be negative"
      }

    }
  };
};

validatorRules.getcharityClaimRules = function() {
  return {
    rules: {
      first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      title: {
        required: true,
        maxlength: 45
      },
      email_address: {
        required: true,
        email: true
      },
      ein: {
        required: true,
        number: true
          // ein: true
      },
      phone_number: {
        required: true,
        phoneAll: true
      },
      acceptTerms: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      title: {
        maxlength: "Title cannot exceed 45 characters"
      },
      acceptTerms: {
        required: "You must check that you agree to our terms and privacy policy."
      }
    },
    errorPlacement: function(error, element) {
      if (element.is("#acceptTerms")) {
        error.appendTo('#acceptTermsError');
      }
    }
  };
};

validatorRules.getExistingCardPaymentRules = function() {
  return {
    rules: {
      donationamount: {
        required: true

      },
      donor_comment:{
        maxlength:255
      },
      onkeyup: false
    },
    messages: {
      donor_comment: {
        maxlength: "Comment cannot exceed 255 characters"
      }
    },
  };
};

validatorRules.getExistingCardPaymentRulesForcanmailing = function() {
  return {
    rules: {
      donationamount: {
        required: true

      },
      donor_comment:{
        maxlength:255
      },
countryCode:{
  required:true,
},
zip: {
  required: true,
 //zipcodeAll: '#country',
zipcodeAll: '#countryCode',
},
      onkeyup: false
    },
    messages: {
      donor_comment: {
        maxlength: "Comment cannot exceed 255 characters"
      }
    },
  };
};


validatorRules.getNewCardPaymentRulesforTicket = function() {
  return {
    rules: {
      name: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      "cc-number": {
        required: true,
        creditcard: true
      },
      "cc-month": {
        required: true,
        min: 01,
        max: 12,
        rangelength: [2, 2]
      },
      "cc-year": {
        required: true,
        rangelength: [4, 4]
      },
      "cc-cvv": {
        required: true,
        rangelength: [3, 4]
      },
      zip: {
        required: true,
        //  zipcodeUS: true
        zipcodeAll: '#country'
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      onkeyup: false
    },
    messages: {
      "name": {
        userfullName: "Full name is required"
      },
      "cc-month": {
        rangelength: "Date should be 2 digits.  Example: '01'"
      },
      "cc-year": {
        rangelength: "Date should be 4 digits.  Example: '2016'"
      },
      "cc-cvv": {
        rangelength: "CVV must be 3 or 4 digits."
      }
    }
  };
};

validatorRules.getNewCardPaymentRules = function() {
  return {
    rules: {
      name: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      "cc-number": {
        required: true,
        creditcard: true
      },
      "cc-month": {
        required: true,
        min: 01,
        max: 12,
        rangelength: [2, 2]
      },
      "cc-year": {
        required: true,
        rangelength: [4, 4]
      },
      "cc-cvv": {
        required: true,
        rangelength: [3, 4]
      },
      "address_1": {
        required: true
      },
      "city": {
        required: true
      },
      countryCode: {
        required: true,
        valueNotEquals: "select"
      },
      state: {
        required: true,
      },
      zip: {
        required: true,
       zipcodeAll: '#country',
        // zipcodeAll: '#countryCode',
        //  zipcodeUS: true
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      donor_comment:{
        maxlength: 255
      },
      onkeyup: false
    },
    messages: {
      "name": {
        userfullName: "Full name is required"
      },
      "donor_comment":{
        maxlength:"Comment can't be exceed 255"
      },
      "cc-month": {
        rangelength: "Date should be 2 digits.  Example: '01'"
      },
      "cc-year": {
        rangelength: "Date should be 4 digits.  Example: '2016'"
      },
      "cc-cvv": {
        rangelength: "CVV must be 3 or 4 digits."
      },
      "address_1": {
        required: "Address 1 is required"
      },
      "city": {
        required: "City is required."
      },
      "state": {
        required: "State is required."
      },
      "country": {
        required: "Country is required"
      },
      "countryCode": {
        required: "Country is required"
      }
    }
  };
};
//validation for  canmailing yes
validatorRules.getNewCardPaymentRulesForCanmailing = function() {
  return {
    rules: {
      name: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      "cc-number": {
        required: true,
        creditcard: true
      },
      "cc-month": {
        required: true,
        min: 01,
        max: 12,
        rangelength: [2, 2]
      },
      "cc-year": {
        required: true,
        rangelength: [4, 4]
      },
      "cc-cvv": {
        required: true,
        rangelength: [3, 4]
      },
      "address_1": {
        required: true
      },
      "city": {
        required: true
      },
      countryCode: {
        required: true,
        valueNotEquals: "select"
      },
      state: {
        required: true,
      },
      zip: {
        required: true,
        // zipcodeAll: '#country',
         zipcodeAll: '#countryCode',
        //  zipcodeUS: true
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      donor_comment:{
        maxlength: 255
      },
      onkeyup: false
    },
    messages: {
      "name": {
        userfullName: "Full name is required"
      },
      "donor_comment":{
        maxlength:"Comment can't be exceed 255"
      },
      "cc-month": {
        rangelength: "Date should be 2 digits.  Example: '01'"
      },
      "cc-year": {
        rangelength: "Date should be 4 digits.  Example: '2016'"
      },
      "cc-cvv": {
        rangelength: "CVV must be 3 or 4 digits."
      },
      "address_1": {
        required: "Address 1 is required"
      },
      "city": {
        required: "City is required."
      },
      "state": {
        required: "State is required."
      },
      "country": {
        required: "Country is required"
      },
      "countryCode": {
        required: "Country is required"
      }
    }
  };
};
validatorRules.stripeDOBDetails = function() {
  return {
    rules: {
      // "cc-day": {
      //   required: true,
      //   min: 01,
      //   max: 31,
      //   rangelength: [2, 2]
      // },
      // "cc-month": {
      //   required: true,
      //   min: 01,
      //   max: 12,
      //   rangelength: [2, 2]
      // },
      "cc-dob": {
        required: true,
        //  rangelength: [4, 4]
      },

      onkeyup: false
    },
    messages: {
      // "cc-day": {
      //   rangelength: "Date should be 2 digits.  Example: '01'"
      // },
      // "cc-month": {
      //   rangelength: "Date should be 2 digits.  Example: '01'"
      // },
      "cc-dob": {
        required: "DOB is required"
      }
    }
  };
};
validatorRules.stripeLegalDetails = function() {
  return {
    rules: {
      "first_name": {
        required: true
      },
      "last_name": {
        required: true
      },
      "business_name": {
        required: true
      },
      "support_url": {
        required: true
      },
      "support_email": {
        required: true
      },
      "support_phone": {
        required: true
      },

      onkeyup: false
    },
    messages: {
      "first_name": {
        required: "First Name is required"
      },
      "last_name": {
        required: "Last Name is required"
      },
      "business_name": {
        required: "Business Name is required"
      },
      "support_url": {
        required: "URL is required"
      },
      "support_email": {
        required: "Email  is required"
      },
      "support_phone": {
        required: "Phone Number is required"
      }

    }
  };
};

validatorRules.stripeBankDetails = function() {
  return {
    rules: {
      account_number: {
        required: true
      },
      account_holder_type: {
        required: true
      },
      onkeyup: false

    },
    messages: {
      account_number: {
        required: "Account Number is required"
      },
      account_holder_type: {
        required: "Account holder type is required"
      }
    }
  }
};
validatorRules.stripeAddressDetails = function() {
  return {
    rules: {
      line1: {
        required: true
      },
      postal_code: {
        required: true
      },
      city: {
        required: true
      },
      state: {
        required: true
      },
      onkeyup: false

    },
    messages: {
      line1: {
        required: "Address1 is required"
      },
      postal_code: {
        required: 'Pincode is required'
      },
      city: {
        required: 'City is required'
      },
      state: {
        required: 'State is required'
      }
    }
  }
};

validatorRules.getGuestInformationRules = function() {
  return {
    rules: {
      full_name: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        phoneAll: true
      },
    },
    messages: {
      full_name: {
        required: "Full name is required"
      },
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      }
    }
  }
};
validatorRules.campaignUpdateValidation = function() {
  return {
    rules: {
      title: {
        required: true
      },
      description: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      title: {
        required: "Title is required"
      },
      description: {
        required: "Description is required"
      }
    }
  };
};
validatorRules.getcardaddingRules = function() {
  return {
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      "cc-number": {
        required: true,
        creditcard: true
      },
      "cc-month": {
        required: true,
        min: 01,
        max: 12,
        rangelength: [2, 2]
      },
      "cc-year": {
        required: true,
        rangelength: [4, 4]
      },
      "cc-cvv": {
        required: true,
        rangelength: [3, 4]
      },
      zip: {
        required: true
          //  zipcodeUS: true
      },
      country: {
        required: true,
        valueNotEquals: "select"
      },
      onkeyup: false
    },
    messages: {
      "cc-month": {
        rangelength: "Date should be 2 digits.  Example: '01'"
      },
      "cc-year": {
        rangelength: "Date should be 4 digits.  Example: '2016'"
      },
      "cc-cvv": {
        rangelength: "CVV must be 3 or 4 digits."
      }
    }
  };
};
validatorRules.getClaimNowSignupRules = function() {
  return {
    rules: {
      charity_name: {
        required: true,
        maxlength: 100,
        allowSpecial: true
      },
      web_url: {
        cus_url: true
      },
      first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      city: {
        required: true
      },
      postal_code: {
        required: true
      },
      acceptTerms: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true
      },
      firstname: {
        required: true
      },
      lastname: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      charity_name: {
        required: "Please enter your Organization Name",
        maxlength: "Organization name should not be more than 100 characters",
        allowSpecial: "Organization name does not allows special characters"
      },
      firstname: {
        required: "FirstName is required"
      },
      lastname: {
        required: "LastName is required"
      },
      acceptTerms: {
        required: "You must check that you agree to our terms and privacy policy."
      },
      web_url: {
        cus_url: "please enter your Organization URL"
      },
      email: {
        email: "please enter valid email",
        required: "Email is required"
      }

    }
  };
};

validatorRules.validateEditOrganizationForm = function() {
  return {
    rules: {
      state: {
        valueNotEquals: "select"
      },
      country: {
        valueNotEquals: "select"
      },
      postal_code: {
        zipcodeAll: '#country'
      },
      onkeyup: false
    }
  };
};

validatorRules.getNonProfitSignupRules = function() {
  return {
    rules: {
      first_name: {
        required: true
      },
      last_name: {
        required: true
      },
      email_address: {
        required: true,
        emailaddress: true
      },
      phone_number: {
        required: true,
        phoneAll: true
      },
      onkeyup: false
    },
    messages: {
      emailaddress: {
        required: "please enter valid email address"
      }
    }
  };
};

validatorRules.validateCampaignEmail = function() {
  return {
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      }
    }
  };
}

validatorRules.getOfflineValidationForm = function() {
  return {
    rules: {
      fullname: {
        required: true,
        userfullName: true
      },
      phone: {
        phoneValid: true
      },
      transaction_date: {
        required: true
      },
      donation: {
        required: true,
        validAmount: true,
        money: true
      },
      email: {
        email: true
      },
      onkeyup: false
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      },
      donation: {
        required: "Donation is required",
        validAmount: "Amount must be greater than 0"
      },
      phone: {
        phoneValid: "Enter valid number"
      }
    }
  };
}
validatorRules.getUserOfflineValidationForm = function() {
  return {
    rules: {
      fullname: {
        required: true,
        userfullName: true
      },
      phone: {
        phoneValid: true
      },
      transaction_date: {
        required: true
      },
      donation: {
        required: true,
        validAmount: true,
        money: true
      },
      code_id: {
        valueNotEquals: "select"
      },
      email: {
        email: true
      },
      onkeyup: false
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      },
      donation: {
        required: "Donation is required",
        validAmount: "Amount must be greater than 0"
      },
      code_id: {
        valueNotEquals: "Please select campaign"
      },
      phone: {
        phoneValid: "Enter valid number"
      }
    }
  };
}
validatorRules.campaignMacrostats = function() {
  return {
    rules: {
      start_date: {
        required: true,
        dateBefore: '#js-macro-end-date'
          //dateIsFuture: true
          //date: true
      },
      end_date: {
        required: true,
        dateAfter: '#js-macro-start-date'
          //limit365Days: true
          //    date: true
      },
      onkeyup: false
    },
    messages: {
      start_date: {

        required: "Start Date is required",
        dateBefore: "Start Date must be less than End Date"

      },
      end_date: {
        required: "End Date is required",
        dateAfter: "End Date must be greater than Start Date"
      }

    }
  }
}
validatorRules.getAddadminRules = function() {
  return {
    rules: {
      yourname: {
        required: true,
        userfullName: true
      },
      onkeyup: false
    }

  };
};

validatorRules.getCreateNewCampaignRulesFundraise = function() {

  return {
    rules: {
      title: {
        required: true
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      suggested_donation: {
        required: true,
        money: true
      },
      goal: {
        required: true,
        money: true
      },

      end_date: {
        dateIsFuture: true,
        limit365Days: true
      },
      campaign_zip: {
        required: true,

      },
      onkeyup: false
    },
    messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Campaign title is required"
      },
      end_date: {
        limit365Days: "End date cannot be more than one year from today",
        dateIsFuture: "Date cannot be in the past"

      },
      goal: {
        required: "Campaign goal amount is required"
      },
      campaign_zip: {
        zipcodeUS: "Zip code is not valid"
      }
    }
  };
};

validatorRules.getValidVideoUrl = function() {
  return {
    rules: {
      code_video_url: {
        youtubevimeo: true
      },
      onkeyup: true
    },
    messages: {
      code_video_url: {
        youtubevimeo: "This is not a valid video URL"
      }
    }
  };
};

validatorRules.getUpdateCampaignRulesFundraise = function() {
  return {
    rules: {
      code_picture_url: {
        required: true,
      },
      code_video_url: {
        youtubevimeo: true
      },
      description: {
        required: true
      },
      onkeyup: false
    },
    messages: {
      code_video_url: {
        youtubevimeo: "This is not a valid video URL"
      },
      description: {
        required: "Campaign description is required"
      }
    }
  };
};

validatorRules.campaignCreateFormValidation = function() {
  return {
    rules: {
      title: {
        required: true,
        minlength: 4,
        maxlength: 255,
        allowSpecial: true
      },
      goal: {
        required: true,
        validAmount: true
      },
      category_id: {
        required: true
      },
      code_text: {
        minlength: 4,
        maxlength: 15
      },
      onkeyup: false
    },
    messages: {
      title: {
        required: "Campaign title is required",
        minlength: "Title length must be at least 4 characters",
        maxlength: "Title length should not exceed 255 characters"
      },
      goal: {
        required: "Campaign goal amount is required",
        validAmount: "Please enter any amount greater than 0"

      },
      category_id: {
        required: "Campaign category is required"
      }
    }
  }
}

validatorRules.beneficiaryFormValidation = function() {
  return {
    rules: {
      firstname: {
        required: true
      },
      lastname: {
        required: true
      },
      beneficiary: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      onkeyup: false
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      },
      firstname: {
        required: "Firstname is required"
      },
      lastname: {
        required: "Lastname is required"
      },
      beneficiary: {
        required: "Beneficiary is required"
      }
    }
  };
}


validatorRules.teamCampaignRules = function() {
  return {
    rules: {
      title: {
        required: true
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      members_invite: {
        required: true,
        multiemails: true
      },
      goal: {
        required: true,
        money: true,
        validAmount: true
      },
      onkeyup: false
    },
    messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Fundraiser name is required"
      },
      goal: {
        required: "Fundraiser goal amount is required",
        validAmount: "Please enter any amount greater than 0"
      },
      members_invite: {
        required: "Please enter at least one email address"
      }
    }
  };
};

validatorRules.teamFundraiseRules = function() {
  return {
    rules: {
      title: {
        required: true
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      goal: {
        required: true,
        money: true,
        validAmount: true
      },
      onkeyup: false
     },
     messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Fundraiser name is required"
      },
      goal: {
        required: "Fundraiser goal amount is required",
        validAmount: "Please enter any amount greater than 0"
      }
    }
   };
  };

validatorRules.reportCampaignRules = function() {
  return {
    rules: {
      fullname: {
        required: true,
        userfullName: true
      },
      email: {
        required: true,
        email: true
      },
      phone:{
        required: true,
        maxlength: 12
      },
      reason: {
        required: true,
        maxlength: 1000
      },
      onkeyup: false
    },
    messages: {
      email: {
        required: "Email is required",
        email: "Please enter valid email address"
      },
      phone: {
        required: "Phone number is required",
        maxlength: "Enter valid number"
      },
      fullname: {
        required: "FullName is required",
        userfullName:"FullName is required"
      },
      reason: {
        required: "Reason is required",
        maxlength: "Reason cannot exceed 1000 characters"
      }
    }
  };
};

validatorRules.escapeFormElements = function(selector) {
  $(selector).find('input').remove();
  $(selector).find('button').remove();
  $(selector).find('textarea').remove();
  $(selector).find('select').remove();
};


validatorRules.teamRules = function() {
  return {
    rules: {
      title: {
        required: true
      },
      teamimage: {
        required: true,
        accept: "image/*"
      },
      team_description: {
        required: true
      },
      custom: {
        required: true,
        maxlength: 450
      },
      code_text: {
        required: true,
        minlength: 4,
        maxlength: 15
      },
      members_invite: {
        required: true,
        multiemails: true
      },
      onkeyup: false
    },
    messages: {
      code_text: {
        minlength: "WeCode must be at least 4 characters",
        maxlength: "WeCode cannot exceed 15 characters",
        required: "All campaigns require a WeCode.  You can enter any alphanumeric text 4-15 characters long."
      },
      title: {
        required: "Team Name is required"
      },
      teamimage: {
        required: "Team image is required"
      },
      team_description: {
        required: "Team description is required"
      },
      members_invite: {
        required: "Please enter at least one email address"
      }
    }
  };
};
