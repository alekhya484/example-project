var logincontrol = can.Control({
    defaults: {
        view: '',
        name: 'narendr',
        login: 'index.html',
        get: '/views/get.html',
        result: '/views/result.handlebars',
        success: '/views/success.mustache',
        welcome: '/views/welcome.mustache'
    }
}, {
    init: function(el) {
        console.log(el);

    },

    '#first click': function(el, ev) {
        //  $(el).text("are fine today?");
        console.log(el);
        console.log(name);
        $(el).html(can.view(this.options.view));
    },
    // for signup
    '#signup click': function(el, ev) {
        ev.preventDefault();
        ev.stopPropagation();
        $('body').html(can.view(this.options.view));

    },
    // back to login page
    '#login1 click': function(el, ev) {
        ev.preventDefault();
        $('body').html(can.view(this.options.login));

    },
    //get the details
    '#check click': function(el, ev) {
        ev.preventDefault();
        //  $('#error').hide();
        var values = $('#getd').serialize();
        //  alert(values);
        indexModel.getdetails(values, this.proxy('getd'));
        //$('body').html(can.view(this.options.login));

    },
    getd: function(res) {
        //  alert("this is getid");
        console.log(res);
        if (res.length >= 1) {
            var ob = {};
            ob.res = res;
            $('body').html(can.view(this.options.result, ob));
        }
        if (res.length == 0) {
            {
                $('body').html(can.view(this.options.get, ob));
                $('#error').show();
            }
        }
    },
    //click for checking details page
    '#get click': function(el, ev) {
        ev.preventDefault();

        $('body').html(can.view(this.options.get));
        $('#error').hide();
    },
    //for values inserting
    '#login click': function(el, ev) {
        ev.preventDefault();
        ev.stopPropagation();
        var values = $('#logindata').serialize();
        console.log(values);
        indexModel.insertingdata(values, this.proxy('afterSuccessfulLogin'));
    },
    afterSuccessfulLogin: function(res) {
          alert("manipulate");
        console.log(res[0]);
        if (res[0] == 'invalid') {
            $('body').html(can.view('index.html'));
            $('#error').show().fadeOut(3000);
        } else {
            $('body').html(can.view(this.options.welcome, {
                name: res[0].username
            }));
        }
    },
    //for boootstrap modal
    '#getmodal click':function(el,ev){
      $('#gettingmodal').show();
    },
    '#modalclose click':function(el,ev){
      $('#gettingmodal').hide();
    },
    // create new record
    '#create click': function(el, ev) {
        ev.preventDefault();
        var values = $('#getsign').serialize();
        alert(JSON.stringify(values))
        indexModel.createaccount(values, this.proxy('create'));
    },
    create: function(res) {
        console.log(res);
        $('#totalform').hide();
        $('.js-main').html(can.view(this.options.success,{name:res.name}));
    },
    //     //not yet impleated
    //     '#login click':function(el,ev){
    //       //  alert("Value: " + $("#name").val());
    //       //  var n=$("#name").val();
    //   $('body').html(can.view('index.html'));
    // },
    '#js-view click': function(el, ev) {
        var va = 19;
        console.log(el);
        AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "titleField": "category",
            "valueField": "column-1",
            "legend": {
                "enabled": true,
                "align": "center",
                "markerType": "circle"
            },
            "titles": [],
            "dataProvider": [{
                "category": "category 1",
                "column-1": va
            }, {
                "category": "category 2",
                "column-1": 20
            }]
        });
    },
    '#sub click':function(el,ev){
      var da=$('#namedetails').serialize();
      $('#gettingmodal').hide();
    },
    '#js-button click': function(el, ev) {
      can.route(':type/:id');
      can.route.ready();
      can.route.attr({type:'login',id:2});
      can.route.bind('change',function(ev,attr,how,oldval,newval){
        console.log("changed the value "+attr);
      })
        console.log(el);
        var verify = '';
        var state=new can.Map({});
        state.attr('a','narendra');
        //stat.profision='job';
        console.log(state.a);
        state.bind('change',function(ev,attr,how,oldval,newval)
      {
        console.log('state changed');
      });
    //  state.attr('b','kumar');
        verify = new verifycontroller('body',{state});
    },


    '#daa click': function(el, ev) {
        // alert("hello");
        var obj = {};
        indexModel.getSomeData(obj, this.proxy('manipulateData'));
    },
    manipulateData: function(res) {
        console.log(res[0].name);
        var obj = {};
        obj.res = res;
        $('#daa').html(can.view(this.options.view, obj));
    },
    '#donate click': function(el, ev) {
        ev.preventDefault();
        var data = $('#donatedata').serialize();
        indexModel.setdonatedata(data, this.proxy('donatedata'));
    },
    donatedata: function(data) {
        console.log(data);
          indexModel.getdonatedata({}, this.proxy('getdonatee'));
    },
    // '#getdonate click': function(el, ev) {
    //     ev.preventDefault();
    //     indexModel.getdonatedata({}, this.proxy('getdonatee'));
    // },
    getdonatee: function(response) {
        console.log(response.length);
        var total = 0;
        for (var i = 0; i <= response.length - 1; i++) {
            total = total + response[i].goal;
        }
        AmCharts.makeChart("chartdiv",
  				{
  					"type": "pie",
  					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  					"titleField": "category",
  					"valueField": "column-1",
  					"allLabels": [],
  					"balloon": {},
  					"legend": {
  						"enabled": true,
  						"align": "center",
  						"markerType": "circle"
  					},
  					"titles": [],
  					"dataProvider": [
  						{
  							"category": "Target",
  							"column-1": 100
  						},
  						{
  							"category": "Collected ",
  							"column-1": total
  						}
  					]
  				}
  			);
      //  $('#js-p1').html(total);
    }

});
